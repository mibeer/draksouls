import java.util.Iterator;

public class Spieler extends Charakter{

    private String spielerklasse;
    private int vitalitaet;
    private int level;
    private int maxGewicht = 10;
    private int gewicht;
    private Gegenstandsliste rucksack;
    private Raum aktuellerRaum;


    public Spieler(String name, int lebensenergie, int angriffspunkte, int verteidigungspunkte, String spielerklasse, int vitalitaet, Raum aktuellerRaum) {
        super(name, lebensenergie, angriffspunkte, verteidigungspunkte);
        this.gewicht = 0;
        this.spielerklasse = spielerklasse;
        this.vitalitaet = vitalitaet;
        this.level = 1;
        this.rucksack = new Gegenstandsliste();
        this.aktuellerRaum = aktuellerRaum;

    }

    public String getSpielerklasse() {

        return spielerklasse;
    }

    public int getVitalitaet() {

        return vitalitaet;
    }

    public void setVitalitaet(int vitalitaet) {

        this.vitalitaet = vitalitaet;
    }

    public int getLevel() {

        return level;
    }

    public int getGewicht() {
        return gewicht;
    }

    /**
     * Gehe durch die Tür mit der gewünschten Richtung.
     * Wenn erfolgreich return true, wenn nicht return false - was darauf
     * hinweisen könnte, dass es keine Tür gibt oder dass die Tür verschlosen ist bzw.
     * der Spieler den Schlüssel nicht besitzt.
     * @param richtung  Richtung in welche der Spieler gehen will
     * @return  True wenn die Tür existiert und der Spieler den Schlüssel dafür besitzt. Ansonsten false
     */
    public boolean geheDurchTuer(String richtung){
        Tuer tuer = aktuellerRaum.getTuer(richtung);
        if(tuer == null){
            return false;
        }

        Raum naechsterRaum = tuer.oeffnen(aktuellerRaum);
        if(naechsterRaum == null){
            //Versperrt - Versuch aufzuschließen.
            Iterator iter = rucksack.iterator();
            while(iter.hasNext() && !tuer.aufschließen((Gegenstand) iter.next()));
        }
        //Versuch die Tür zu öffnen.
        naechsterRaum = tuer.oeffnen(aktuellerRaum);
        if(naechsterRaum != null){
            betreteRaum(naechsterRaum);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Betrete den ausgewählten Raum
     * @param raum
     */
    public void betreteRaum (Raum raum){

        aktuellerRaum = raum;
    }

    /**
     * Gibt den aktuellen Raum des Spielers zurück.
     * @return  aktuellen Raum des Spielers.
     */
    public Raum getAktuellenRaum(){

        return aktuellerRaum;
    }

    /**
     * Gibt eine lange Beschreibung der Gegenstände welche der Spieler in seinem Rucksack hat.
     * @return Lange Beschreibung der Gegenstände im Rucksack.
     */
    public String getGegenstaendeString(){

        return "Du trägst: " + rucksack.getLangeBeschreibung();
    }

    /**
     * Gibt eine lange Beschreibung der Gegenstände in einem Raum.
     * @return Lange Beschreibung der Gegenstände in einem Raum.
     */
    public String getLangeBeschreibung(){
        String returnString = aktuellerRaum.gibLangeBeschreibung();
        returnString += "\n" + getGegenstaendeString();
        return returnString;
    }

    /**
     * Methode zum aufheben von Gegenständen welche sich im Raum befinden.
     * @param gegenstandsname   Der Name des aufzuhebenden Gegenstandes.
     * @return  Der Gegenstand welcher aufgehoben wird.
     */
    public Gegenstand gegenstandAufheben (String gegenstandsname){


            if (kannGegenstandAufheben(gegenstandsname)) {
                Gegenstand gegenstand = aktuellerRaum.raumgegenstandEntfernen(gegenstandsname);
                rucksack.hinzufuegen(gegenstandsname, gegenstand);
                this.gewicht += gegenstand.getGewicht();
                if (gegenstand.getName() == "Schwert"){
                    this.setAngriffspunkte(this.getAngriffspunkte()+gegenstand.getWaffenAngriffspunkte());
                }
                if (gegenstand.getName() == "Rüstung" || gegenstand.getName() == "Helm"){
                    this.setVerteidigungspunkte(this.getVerteidigungspunkte()+gegenstand.getRuestungverteidigungspunkte());
                }
                return gegenstand;
            }
        return null;
    }

    /**
     * Prüft ob der Spieler den Gegenstand aufheben kann.
     * @param gegenstandsname Name des Gegenstandes
     * @return True wenn der Gegenstand aufgehoben werden kann. Ansonsten false.
     */
    private boolean kannGegenstandAufheben (String gegenstandsname)  {

        boolean kannAufheben = true;
        Gegenstand gegenstand = aktuellerRaum.getRaumgegenstaende(gegenstandsname);
        if (gegenstand == null){
            System.out.println("Der Gegenstand " + gegenstandsname + " befindet sich nicht in diesem Raum."+"\n");
            kannAufheben = false;
        }
        else {
            gewicht = rucksack.getGesamtgewicht() + gegenstand.getGewicht();
            if (gewicht > maxGewicht){
                System.out.println("Du bist zu schwer und kannst den Gegenstand " + gegenstandsname + " nicht aufnehmen."+"\n");
                System.out.println();
                kannAufheben = false;
            }
        }
        return kannAufheben;
    }

    /**
     * Methode zum ablegen eines Gegenstandes.
     * @param gegenstandsname   Name des Gegenstandes.
     * @return  Der Gegenstand der abgelegt wird.
     */
    public Gegenstand gegenstandFallenlassen (String gegenstandsname) {

        Gegenstand gegenstand = rucksack.entfernen(gegenstandsname);
        this.gewicht -= gegenstand.getGewicht();
        if (gegenstand != null) {
            aktuellerRaum.raumgegenstandHinzufuegen(gegenstand);
            if (gegenstand.getName().equals("Schwert")) {
                this.setAngriffspunkte(this.getAngriffspunkte() - gegenstand.getWaffenAngriffspunkte());
            }
            if (gegenstand.getName().equals("Rüstung")  || gegenstand.getName().equals("Helm")) {
                this.setVerteidigungspunkte(this.getVerteidigungspunkte() - gegenstand.getRuestungverteidigungspunkte());
            }
        }
        return gegenstand;
    }

    /**
     * Verwenden eines Heiltrankes.
     */
    public void nimmHeiltrank() {

        if (rucksack.getGegenstand("Heiltrank") != null){
            if (this.getLebensenergie()+3 < this.getMaxLebensenergie()){
                this.setLebensenergie(this.getLebensenergie()+3);
            }else {
                this.setLebensenergie(this.getMaxLebensenergie());
            }
            this.gewicht -= rucksack.getGegenstand("Heiltrank").getGewicht();
            this.rucksack.entfernen("Heiltrank");
        }
    }

    /**
     * Verwenden einer Seele welche zum aufleveln benötigt wird.
     * @param angriff
     * @param verteidigung
     * @param vitalitaet
     */
    public void verwendeSeele(boolean angriff, boolean verteidigung, boolean vitalitaet){

        if (rucksack.getGegenstand("Seele")!= null){
            this.levelAufstieg(angriff, verteidigung, vitalitaet);
            this.gewicht -= rucksack.getGegenstand("Seele").getGewicht();
            rucksack.entfernen("Seele");
        }
    }

    /**
     * Methode zum aufleveln des Spielers.
     * @param angriff
     * @param verteidigung
     * @param vitalitaet
     */
    public void levelAufstieg(boolean angriff, boolean verteidigung, boolean vitalitaet){

        if (angriff == true && verteidigung == false && vitalitaet == false){
            System.out.println("Angriff wurde um 1 erhöht!\n");
            this.setAngriffspunkte(this.getAngriffspunkte()+1);
        }else if (angriff == false && verteidigung == true && vitalitaet == false){
            System.out.println("Verteidigung wurde um 1 erhöht!\n");
            this.setVerteidigungspunkte(this.getVerteidigungspunkte()+1);
        }else if (angriff == false && verteidigung == false && vitalitaet == true){
            System.out.println("Vitalität wurde um 1 erhöht!\n");
            this.setVitalitaet(this.getVitalitaet()+1);
            this.maxGewicht += 2;
        }
        this.level += 1;
        this.setMaxLebensenergie(this.getMaxLebensenergie()+1);
        this.setLebensenergie(this.getMaxLebensenergie());
    }


    @Override
    public String toString() {
        return "---- "+getName() + "'s Spielerwerte ----\n" +
                "Level: " + this.getLevel() + "\n" +
                "Spielerklasse: " + this.getSpielerklasse() + "\n" +
                "Lebensenergie: " + this.getLebensenergie() + "\n" +
                "Angriffspunkte: " + this.getAngriffspunkte() + "\n" +
                "Verteidigungspunkte: " + this.getVerteidigungspunkte() +"\n" +
                "Vitalitaet: " + this.getVitalitaet() + "\n" +
                "Gewicht: " + this.getGewicht() +"\n";
    }

}
