import java.util.Scanner;


public class Kommandozeilenmenu {

    private Spieler spieler;
    private Raum leuchtfeuer, pfad, eingang, kerker, verließ, kerkergang, folterkammer,kerkermeister,aufgang, innenhof, flur, bibliothek, halle, endboss;
    private Gegenstand schwert, helm, ruestung, endbossschluessel, heiltrank, seele;
    private Gegner ork, skelett, taurus, hexe, pyromant, ritter, daemon;
    Scanner scan;

    public Kommandozeilenmenu() {
        this.scan = new Scanner(System.in);
        this.raeumeAnlegen();
        this.spielerAuswahl();

    }

    public void MenüAnzeigen() {
        System.out.println("MENÜ");
        System.out.println("1) Gehe nach ");
        System.out.println("2) Im Raum umsehen ");
        System.out.println("3) Zeige Spielerwerte ");
        System.out.println("4) Zeige Rucksack ");
        System.out.println("5) Gegenstand aufheben ");
        System.out.println("6) Gegenstand fallen lassen");
        System.out.println("7) Verwende Gegenstand");
        System.out.println("0) Spiel beenden");
    }

    public void start() {

        willkommenstextAusgeben();

            String s = "_";
            String m = "_";

            while (!s.equals("0")) {

                MenüAnzeigen();
                s = scan.nextLine();
                switch (s) {
                    case "1":
                        this.geheNach();
                        break;
                    case "2":
                        this.umsehen();
                        break;
                    case "3":
                        this.zeigeSpielerwerte();
                        break;
                    case "4":
                        this.zeigeRucksack();
                        break;
                    case "5":
                        this.aufheben();
                        break;
                    case "6":
                        this.ablegen();
                        break;
                    case "7":
                        this.gegenstandVerwenden();
                        break;
                    case "0":
                        break;
                    default:
                        this.fehler();
                        break;
                }
            }
        scan.close();
       }

    /**
     * Spieler bewegt sich im Spiel.
     */
    public void geheNach() {

        System.out.println("In welche Richtung willst du gehen?");
        System.out.println("0) Zurück ins Menu 1) Westen 2) Süden 3) Osten 5) Norden");
        String s1 ="_";

        while (!s1.equals("0")){
            s1 = scan.nextLine();
            switch(s1){
                case "1":
                    wechsleRaum("Westen");
                    s1 = "0";
                    break;
                case"2":
                    wechsleRaum("Süden");
                    s1="0";
                    break;
                case"3":
                    wechsleRaum("Osten");
                    s1 = "0";
                    break;
                case"5":
                    wechsleRaum("Norden");
                    s1 = "0";
                case"0":
                    break;
                default:
                    System.out.println("Gib die gewünschte Richtung an.\n");
                    break;
            }
        }
    }

    /**
     * Spieler kann sich im Raum umsehen.
     */
    public void umsehen()
    {
        System.out.println();
        System.out.println(spieler.getAktuellenRaum().toString());
        System.out.println();
    }

    /**
     * Zeigt die Spielerwerte.
     */
    public void zeigeSpielerwerte(){
        System.out.println();
        System.out.println(spieler.toString());
    }

    /**
     * Zeigt den Inhalt des Rucksackes des Spielers.
     */
    public void zeigeRucksack(){

        System.out.println();
        System.out.println(spieler.getGegenstaendeString()+"\n");
    }

    /**
     * Aufheben eines Gegenstandes aus dem Raum.
     */
    public void aufheben(){

        System.out.println("Welchen Gegenstand willst du aufheben?");
        System.out.println(spieler.getAktuellenRaum().getRaumgegenstaendeString());
        System.out.println("0) Zurück 1) Heiltrank 2) Seele 3) Schwert 4) Rüstung 5) Helm 6) Schlüssel");
        String aufzuhebenderGegenstand = "_";

        while (!aufzuhebenderGegenstand.equals("0")){
            aufzuhebenderGegenstand = scan.nextLine();

            switch(aufzuhebenderGegenstand){
                case"1":
                    spieler.gegenstandAufheben("Heiltrank");
                    aufzuhebenderGegenstand = "0";
                    break;
                case "2":
                    spieler.gegenstandAufheben("Seele");
                    aufzuhebenderGegenstand = "0";
                    break;
                case "3":
                    spieler.gegenstandAufheben("Schwert");
                    aufzuhebenderGegenstand = "0";
                    break;
                case "4":
                    spieler.gegenstandAufheben("Rüstung");
                    aufzuhebenderGegenstand = "0";
                    break;
                case "5":
                    spieler.gegenstandAufheben("Helm");
                    aufzuhebenderGegenstand = "0";
                    break;
                case "6":
                    spieler.gegenstandAufheben("Schlüssel");
                    aufzuhebenderGegenstand="0";
                    break;
                case "0":
                    break;
                default:
                    this.fehler();
                    break;
            }
        }
    }

    /**
     * Ablegen eines Gegenstandes aus dem Rucksack.
     */
    public void ablegen(){

        System.out.println("Welchen Gegenstand willst du ablegen?");
        System.out.println("0) Zurück 1) Heiltrank 2) Seele 3) Schwert 4) Rüstung 5) Helm");
        System.out.println(spieler.getGegenstaendeString());
        String abzulegenderGegenstand = "_";

        while (!abzulegenderGegenstand.equals("0")){
            abzulegenderGegenstand = scan.nextLine();

            switch(abzulegenderGegenstand){
                case "1":
                    spieler.gegenstandFallenlassen("Heiltrank");
                    abzulegenderGegenstand = "0";
                    break;
                case "2":
                    spieler.gegenstandFallenlassen("Seele");
                    abzulegenderGegenstand = "0";
                    break;
                case "3":
                    spieler.gegenstandFallenlassen("Schwert");
                    abzulegenderGegenstand = "0";
                    break;
                case "4":
                    spieler.gegenstandFallenlassen("Rüstung");
                    abzulegenderGegenstand = "0";
                    break;
                case "5":
                    spieler.gegenstandFallenlassen("Helm");
                    abzulegenderGegenstand = "0";
                    break;
                case "0":
                    break;
                default:
                    this.fehler();
                    break;
            }
        }
    }

    /**
     * Verwenden eines Gegenstandes aus dem Rucksack.
     */
    public void gegenstandVerwenden(){

        System.out.println("Welchen Gegenstand willst du verwenden?");
        System.out.println(spieler.getGegenstaendeString());
        System.out.println("0) zurück 1) Heiltrank 2) Seele");
        String verwendeterGegenstand ="_";

        while(!verwendeterGegenstand.equals("0")){
            verwendeterGegenstand = scan.nextLine();

            switch (verwendeterGegenstand){
                case "1":
                    spieler.nimmHeiltrank();
                    verwendeterGegenstand ="0";
                    break;
                case "2":
                    this.waehleAttribut();
                    verwendeterGegenstand="0";
                    break;
                case"0":
                    break;
                default:
                    this.fehler();
                    break;
            }
        }
    }

    public void fehler()
    {
        System.out.println("Dieser Befehl ist nicht gültig!");
    }

    /**
     * Spieler wählen lassen welches Attribut er bei einem Levelaufstieg erhöhen will.
     */
    private void waehleAttribut(){

        System.out.println("Welches Eigenschaft möchtes du aufleveln?");
        System.out.println("1) Angriff 2) Verteidigung 3) Vitalität");
        System.out.println(spieler.toString());
        String attribut = "_";

        while(!attribut.equals("0")){
            attribut = scan.nextLine();
            switch (attribut){
                case "1":
                    spieler.verwendeSeele(true, false, false);
                    attribut ="0";
                    break;
                case "2":
                    spieler.verwendeSeele(false,true,false);
                    attribut ="0";
                    break;
                case "3":
                    spieler.verwendeSeele(false, false,true);
                    attribut ="0";
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Bitte gib eine Eigenschaft an.\n");
                    break;
            }
        }
    }


    /**
     * Spieler wählt seinen Namen und Spielerklasse.
     */
    public void spielerAuswahl() {
        System.out.println("---- Wähle deinen Spielernamen ----");
        String spielerName = scan.nextLine();
        System.out.println("---- Wähle deine Spielerklasse ----");
        System.out.println("1) Ritter 2) Barbar 3) Dieb");
        String s1 = "_";

        while (!s1.equals("0")) {
            s1 = scan.nextLine();
            switch (s1) {
                case "1":
                    spieler = new Spieler(spielerName,10,7,8,"Ritter",5,leuchtfeuer);
                    s1="0";
                    break;
                case "2":
                    spieler = new Spieler(spielerName,10,8,7,"Barbar",7,leuchtfeuer);
                    s1="0";
                    break;
                case "3":
                    spieler = new Spieler(spielerName,10,6,7,"Dieb", 8,leuchtfeuer);
                    s1="0";
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Bitte wählen deinen Charakter aus!");
                    break;
            }
        }

    }

    /**
     * Räume, Türen und Gegner werden initialisiert und gesetzt.
     * @return  Den Startpunkt des Spiels.
     */
    private Raum raeumeAnlegen(){

        //Räume anlegen
        leuchtfeuer = new Raum(" am Leuchtfeuer.");
        pfad = new Raum(" am Pfad der Opferung.");
        eingang = new Raum(" am Eingang zum Kerker.");
        kerker = new Raum(" im Kerker von Irithyll.");
        verließ = new Raum(" in einem Verließ.");
        kerkergang = new Raum(" im Kerkergang.");
        folterkammer = new Raum(" in der Folterkammer.");
        kerkermeister = new Raum (" im Raum des Kerkermeisters.");
        aufgang = new Raum(" im Schlossaufgang zu Anor Londo.");
        innenhof = new Raum (" im Innenhof.");
        flur    = new Raum (" in einem Flur.");
        bibliothek = new Raum(" in der Bibliothek der schwarzen Künste.");
        halle = new Raum (" in der großen Halle von Anor Londo.");
        endboss = new Raum (" in Dämon Aldirs Gemach.");


        //Türen/Ausgänge initialisieren
        new Tuer(leuchtfeuer,"Süden",pfad,"Norden",null,false);
        new Tuer(pfad,"Süden", eingang, "Norden", null,false);
        new Tuer(eingang,"Osten", kerker,"Westen",null,false);
        new Tuer(kerker,"Osten",verließ,"Westen",null,false);
        new Tuer(kerker,"Süden",kerkergang,"Norden",null,false);
        new Tuer(kerkergang,"Osten",folterkammer,"Westen",null,false);
        new Tuer(kerkergang,"Westen",kerkermeister,"Osten",null,false);
        new Tuer(kerkermeister,"Süden",aufgang,"Norden",null,false);
        new Tuer(aufgang,"Osten",innenhof,"Westen",null,false);
        new Tuer(innenhof,"Norden",flur,"Süden", null,false);
        new Tuer(flur,"Westen",bibliothek,"Osten", null,false);
        new Tuer(flur,"Osten",halle,"Westen",null,false);
        new Tuer(halle,"Süden",endboss,"Norden",endbossschluessel,true);


        //Gegenstände initialisieren
        schwert = new Gegenstand("Schwert", "Claymore von Pharis",3,3,0,0);
        helm = new Gegenstand("Helm","Drachentöter-Helm",1,0,1,0);
        ruestung = new Gegenstand("Rüstung","Drachentöter-Rüstung",3,0,3,0);
        endbossschluessel = new Gegenstand("Schlüssel","Schlüssel zu Dämon Aldirs Gemach",1,0,0,0);
        heiltrank = new Gegenstand("Heiltrank","Heiltrank der Leben regeneriert",1,0,0,5);
        seele = new Gegenstand("Seele","Eine leuchtende Seele zum aufleveln",1,0,0, 10);


        //Gegenstände in Räumen platzieren
        pfad.raumgegenstandHinzufuegen(schwert);
        eingang.raumgegenstandHinzufuegen(helm);
        kerker.raumgegenstandHinzufuegen(seele);
        verließ.raumgegenstandHinzufuegen(ruestung);
        kerkergang.raumgegenstandHinzufuegen(heiltrank);
        aufgang.raumgegenstandHinzufuegen(heiltrank);
        flur.raumgegenstandHinzufuegen(heiltrank);
        flur.raumgegenstandHinzufuegen(seele);
        bibliothek.raumgegenstandHinzufuegen(endbossschluessel);
        halle.raumgegenstandHinzufuegen(seele);


        //Gegner initialisieren
        ork = new Gegner("Ork", 7,4,10);
        skelett = new Gegner("Skelett-Krieger",9,5,5 );
        taurus = new Gegner("Taurus Kerkerwächter von Irithyll",10,5,10);
        hexe = new Gegner("Hexe von Hemwick",9,7,7);
        pyromant = new Gegner("Pyromant", 11,11,9);
        ritter = new Gegner("Schwarzer Ritter",10,10,10);
        daemon = new Gegner("Aldir der Dämonenkönig",13,10,10);

        //Gegner setzen
        eingang.setGegner(ork);
        folterkammer.setGegner(skelett);
        kerkermeister.setGegner(taurus);
        innenhof.setGegner(hexe);
        bibliothek.setGegner(pyromant);
        halle.setGegner(ritter);
        endboss.setGegner(daemon);


        return leuchtfeuer;
    }

    /**
     * Willkommenstext des Spiels.
     */
    private void willkommenstextAusgeben(){
        System.out.println("...alles beginnt mit der Glut...");
        sleep(1000);
        System.out.println("Und mit dem Feuer die Kontraste");
        sleep(1000);
        System.out.println("Hitze und Kälte");
        sleep(1000);
        System.out.println("Leben und Tod");
        sleep(1000);
        System.out.println("Und natürlich ... Licht und Finsternis");
        sleep(1500);
        System.out.println("Begib dich auf die Suche nach der heiligen Glut und erfülle dein Schicksal.\n");
        sleep(2000);
        System.out.println(spieler.getLangeBeschreibung()+"\n");

    }

    /**
     * Methode um den Willkommenstext mit einer Zeitverzögerung auszugeben.
     * @param millis
     */
    private void sleep(long millis){
        try {
            Thread.sleep(millis);
        }catch (InterruptedException ignored){

        }
    }

    /**
     * Ausgabe bei Tot des Spielers.
     */
    private void printTot(){

        System.out.println("\nDu bist tot! GAME OVER!");
        System.exit(0);
    }

    /**
     * Ausgabe bei Sieg des Spielers.
     */
    private void printSieg(){

        System.out.println("\nDu hast gewonnen! GG");
        System.exit(0);
    }

    /**
     * Versuch den Raum zu wechseln. Falls es einen Ausgang gibt wird der neue Raum betreten.
     * Anderenfalls wird eine Fehlermeldung ausgegeben.
     * @param richtung  Richtung in welche der Spieler gehen will.
     */
    private void wechsleRaum(String richtung){

        //Wir versuchen den Raum zu verlassen.
        Tuer tuer = spieler.getAktuellenRaum().getTuer(richtung);

        if (tuer == null){
            System.out.println("Du kannst in diese Richtung nicht gehen!\n");
        }else{
            if(spieler.geheDurchTuer(richtung)){
                System.out.println(spieler.getAktuellenRaum().toString());
                if (spieler.getAktuellenRaum().getGegner()!=null){
                    System.out.println("Stelle dich " + spieler.getAktuellenRaum().getGegner().getName());
                    this.kampf();
                }
            }else {
                System.out.println("Die Türe ist versperrt und du hast keinen Schlüssel dafür!");
            }
        }
    }

    /**
     * Kampfmethode um Spieler und Gegener kämpfen zu lassen.
     */
    private void kampf(){

        boolean ende = false;
        while (!ende){
            Gegner aktuellerGegner = spieler.getAktuellenRaum().getGegner();
            while(spieler.getLebensenergie()>0 && aktuellerGegner.getLebensenergie()>0 ){
                aktuellerGegner.schadenNehmen(spieler.getAngriffspunkte());
                if(aktuellerGegner.getLebensenergie()>0) {
                    spieler.schadenNehmen(aktuellerGegner.getAngriffspunkte());
                }

                if(spieler.getLebensenergie()<=0){
                    this.printTot();
                }
                if(aktuellerGegner.getLebensenergie()<=0){
                    System.out.println("Du hast den Kampf gegen " + aktuellerGegner.getName() + " gewonnen!\n");
                    spieler.getAktuellenRaum().gegnerEntfernen(aktuellerGegner);
                    if(aktuellerGegner == daemon){
                        printSieg();
                    }
                }
            }
            ende = true;
        }
    }




}
