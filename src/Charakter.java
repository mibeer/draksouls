public abstract class Charakter extends GameObject {


    private int lebensenergie;
    private int maxLebensenergie;
    private int angriffspunkte;
    private int verteidigungspunkte;

    public Charakter(String name, int lebensenergie, int angriffspunkte, int verteidigungspunkte) {
        super(name);
        this.lebensenergie = lebensenergie;
        this.maxLebensenergie = lebensenergie;
        this.angriffspunkte = angriffspunkte;
        this.verteidigungspunkte = verteidigungspunkte;
    }


    public int getLebensenergie()
    {

        return lebensenergie;
    }

    public void setLebensenergie(int lebensenergie) {

        this.lebensenergie = lebensenergie;
    }

    public int getMaxLebensenergie() {

        return maxLebensenergie;
    }

    public void setMaxLebensenergie(int maxLebensenergie) {

        this.maxLebensenergie = maxLebensenergie;
    }

    public int getAngriffspunkte() {

        return angriffspunkte;
    }

    public void setAngriffspunkte(int angriffspunkte) {

        this.angriffspunkte = angriffspunkte;
    }

    public int getVerteidigungspunkte() {

        return verteidigungspunkte;
    }

    public void setVerteidigungspunkte(int verteidigungspunkte) {

        this.verteidigungspunkte = verteidigungspunkte;
    }

    /**
     * Methode um Charakter Schaden durch einen Angriff zuzufügen.
     * @param schaden   Der erlittene Schaden.
     */
    public void schadenNehmen (int schaden){
        int tatSchaden;
        if(schaden > this.getVerteidigungspunkte()){
            tatSchaden = schaden-this.getVerteidigungspunkte();
            this.lebensenergie -= tatSchaden;
        }else {
            this.lebensenergie -= 1;
        }
    }


}
