import java.util.HashMap;
import java.util.Set;

public class Raum extends GameObject{


    private HashMap<String, Tuer> tueren;    //die Ausgänge/Türen dieses Raums
    private Gegenstandsliste raumgegenstaende;  //Speichert Gegenstände welche im Raum verfügbar sind.
    private Gegner gegner;

    public Raum(String name){
        super(name);
        this.tueren = new HashMap<>();
        this.raumgegenstaende = new Gegenstandsliste();

    }

    public void setGegner(Gegner gegner){

        this.gegner = gegner;
    }

    public Gegner getGegner(){

        return gegner;
    }

    /**
     * Den Gegner aus einem Raum entfernen.
     * @param gegner Der im Raum platzierte Gegner.
     */
    public void gegnerEntfernen(Gegner gegner){

        this.gegner = null;
    }


    /**
     * Definiere einen Ausgang für diesen Raum.
     * @param richtung  die Richtung in der der Ausgang liegen soll.
     * @param tuer   der Raum, der über diesen Ausgang erreicht wird.
     */
    public void setTuer(String richtung, Tuer tuer){

        tueren.put(richtung, tuer);
    }

    /**
     * Liefere den Raum, den wir erreichen, wenn wir aus diesem Raum in die angegebene Richtung gehen.
     * Liefere 'null', wenn in dieser Richtung kein Ausgang ist.
     * @param richtung  Die Richtung in die gegangen werden soll.
     * @return  Den Raum in der angegeben Richtung.
     */
    public Tuer getTuer(String richtung)
    {
        return tueren.get(richtung);
    }

    /**
     * Gibt den Namen bzw. die kurze Beschreibung des Raumes zurück.
     * @return  Die kurze Beschreibung dieses Raums.
     */
    public String getRaumName(){

        return this.getName();
    }

    /**
     *  Gibt eine lange Beschreibung des Raumes zurück.
     * @return  Die lange Beschreibung dieses Raums.
     */
    public String gibLangeBeschreibung(){

        return "Du befindest dich " + this.getRaumName() + ".\n" + gibAusgaengeAlsString();
    }

    /**
     * Liefere eine Zeichenkette, die die Ausgänge dieses Raums beschreibt.
     * @return  Eine Beschreibung der Ausgänge dieses Raums.
     */
    private String gibAusgaengeAlsString() {

        String ergebnis = "Ausgänge: ";
        Set<String> keys = tueren.keySet();
        for (String ausgang : keys)
            ergebnis += " " + ausgang;
        return ergebnis;
    }


    /**
     * Gibt den Gegenstand in einem Raum zurück falls einer vorhanden ist.
     * Ansonsten wird null zurückgegeben.
     * @param name  Der Name des Gegenstandes der zurückgegeben wird.
     * @return      Der Gegenstand oder null wenn er nicht im Raum ist.
     */
    public Gegenstand getRaumgegenstaende(String name) {

        return raumgegenstaende.getGegenstand(name);
    }

    /**
     * Gibt die Raumgegenstände als lange Beschreibung zurück.
     * @return  Lange Beschreibung der Raumgegenstände
     */
    public String getRaumgegenstaendeString(){

        return raumgegenstaende.getKurzeBeschreibung() + raumgegenstaende.getLangeBeschreibung();
    }


    /**
     * Entfernt den Gegenstand und gibt ihn zurück wenn er verfügbar ist.
     * @param name Name des Gegenstandes der entfernt wird.
     * @return     Der abgelegte Gegenstand.
     */
    public Gegenstand raumgegenstandEntfernen(String name){

        return raumgegenstaende.entfernen(name);
    }

    /**
     * Hinzufügen eines Gegenstandes in einem Raum.
     * @param gegenstand    Der Gegenstand der hinzugefügt wird.
     */
    public void raumgegenstandHinzufuegen (Gegenstand gegenstand){
        this.raumgegenstaende.hinzufuegen(gegenstand.getName(),gegenstand);
    }



    @Override
    public String toString() {
        return "Du befindest dich " + this.getName() +
                "\nEs gibt diese " + gibAusgaengeAlsString() +
                "\nHier befinden sich folgende Gegenstände: " + raumgegenstaende.getKurzeBeschreibung()+ raumgegenstaende.getLangeBeschreibung()+"\n";
    }

}
