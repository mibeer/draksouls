import java.util.HashMap;
import java.util.Iterator;

/**
 * Diese Klasse hält eine Liste der Gegenstände des Spielers.
 */
public class Gegenstandsliste {


    private HashMap <String, Gegenstand> gegenstandsliste;


    public Gegenstandsliste() {

        this.gegenstandsliste = new HashMap<>();
    }

    public Iterator<Gegenstand> iterator()
    {

        return gegenstandsliste.values().iterator();
    }

    /**
     * Gegenstand wird aus der Lieste entfernt.
     *
     * @param name  Name des Gegenstandes
     * @return      Der entfernte Gegenstand.
     */
    public Gegenstand entfernen(String name)
    {
        return gegenstandsliste.remove(name);
    }

    /**
     * Hinzufügen eines Gegenstandes.
     *
     * @param name  Name des Gegenstandes.
     * @param gegenstand    Der Gegendstand der hinzugefügt wird.
     */
    public void hinzufuegen(String name, Gegenstand gegenstand)
    {
        gegenstandsliste.put(name, gegenstand);
    }

    /**
     * Gibt den Namen des Gegenstandes zurück.
     *
     * @param name  Name des Gegenstandes.
     * @return      Den Namen des Gegenstandes.
     */
    public Gegenstand getGegenstand(String name)
    {

        return gegenstandsliste.get(name);
    }

    /**
     * Gibt eine Zeichenkette zurück, welche eine lange Beschreibung der
     * Gegenstände in der Liste enthält.
     *
     * @return  Lange Beschreibung des Gegenstandes.
     */
    public String getLangeBeschreibung()
    {
        String returnString = "";
        for (Iterator<Gegenstand> iter = gegenstandsliste.values().iterator(); iter.hasNext();)
            returnString += "  " + iter.next().getBeschreibung();

        return returnString;
    }

    /**
     * Gibt eine Zeichenkette zurück, welche die Namen der Gegenstände in der Liste enthält.
     *
     * @return  Kuzre Beschreibung des Gegenstandes.
     */
    public String getKurzeBeschreibung()
    {
        String returnString = "";
        for (Iterator<Gegenstand> iter = gegenstandsliste.values().iterator(); iter.hasNext();)
            returnString += " " + iter.next().getName();

        return returnString;
    }

    /**
     * Gibt das Gesamtgewicht der Gegenstände in der Liste zurück.
     *
     * @return Gesamtgewicht
     */
    public int getGesamtgewicht()
    {
        int gewicht = 0;
        for (Iterator<Gegenstand> iter = gegenstandsliste.values().iterator(); iter.hasNext();){
            gewicht += iter.next().getGewicht();
        }
        return gewicht;
    }



}
