public class Gegenstand extends GameObject {


    private String beschreibung;
    private int gewicht;
    private int waffenAngriffspunkte;
    private int ruestungverteidigungspunkte;
    private int heilungspunkte;



    public Gegenstand(String name, String beschreibung, int gewicht, int waffenAngriffspunkte, int ruestungverteidigungspunkte, int heilungspunkte) {

        super(name);
        this.beschreibung = beschreibung;
        this.gewicht = gewicht;
        this.waffenAngriffspunkte = waffenAngriffspunkte;
        this.ruestungverteidigungspunkte = ruestungverteidigungspunkte;
        this.heilungspunkte = heilungspunkte;

    }

    public String getBeschreibung() {

        return beschreibung;
    }

    public int getGewicht() {

        return gewicht;
    }

    public int getWaffenAngriffspunkte() {

        return waffenAngriffspunkte;
    }

    public int getRuestungverteidigungspunkte() {

        return ruestungverteidigungspunkte;
    }

}
