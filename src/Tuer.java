public class Tuer {

    private Raum raum1;
    private Raum raum2;
    private String richtung1;
    private String richtung2;
    private Gegenstand schluessel;
    private boolean sperren;

    public Tuer(Raum raum1, String richtung1, Raum raum2, String richtung2, Gegenstand schluessel, boolean sperren) {

        this.raum1 = raum1;
        this.raum2 = raum2;
        this.richtung1 = richtung1;
        this.richtung2 = richtung2;
        raum1.setTuer(richtung1,this);
        raum2.setTuer(richtung2,this);
        this.schluessel = schluessel;
        this.sperren = sperren;
    }



    /**
     * Versuch die Tür mit dem Schlüssel aufzuschließen.
     * @param schluessel
     * @return  true wenn die Tür erfolgreich aufgeschlossen wurde.
     */

    public boolean aufschließen(Gegenstand schluessel){
        if (this.schluessel == schluessel && schluessel != null){
            sperren = false;
        }
        return sperren=false;
    }


    /**
     * Versuch von einem Raum in den anderen durch die Tür zu gehen.
     * @param vonRaum Der Raum von dessen Seite der Spieler versucht aufzuschließen.
     * @return  Der Raum auf der anderen Seite oder null wenn die Tür
     *          versperrt ist.
     */
    public Raum oeffnen (Raum vonRaum){

        if (sperren){
            return null;
        }
        if (vonRaum == raum1){
            return raum2;
        }else if (vonRaum == raum2){
            return  raum1;
        }else{
            return null;
        }
    }

}
